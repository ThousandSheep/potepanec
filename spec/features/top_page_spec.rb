require 'rails_helper'
RSpec.feature "Top_page_spec" do
  given(:taxonomy_1) { create :taxonomy, name: 'Categories' }
  given(:taxon_1) { create :taxon, name: 'C1', taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id }

  given!(:product_1) { create :product, available_on: Time.current.ago(1.week), taxons: [taxon_1] }
  # 「新着商品」はavailable_onが１ヶ月以内なので、ギリギリの新着商品を設定する
  given!(:product_2) { create :product, available_on: Time.current.ago(1.month) + 1.day }
  given!(:product_3) { create :product, available_on: Time.current.ago(1.year) }

  before do
    visit potepan_path
  end

  scenario "show new arrival products" do
    expect(find('.row.featuredProducts')).to have_content(product_1.name.upcase)
    expect(find('.row.featuredProducts')).to have_content(product_2.name.upcase)
    expect(find('.row.featuredProducts')).to have_no_content(product_3.name.upcase)
  end

  scenario "render product page when click on new arrival product" do
    within '.row.featuredProducts' do
      click_on(product_1.name)
    end
    expect(page).to have_selector 'h2', text: product_1.name.upcase
  end
end
