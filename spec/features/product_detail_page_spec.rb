require 'rails_helper'
RSpec.feature "Product_detail_page_spec", type: :feature do
  given(:taxonomy_1) { create :taxonomy, name: 'Categories' }
  given(:taxonomy_2) { create :taxonomy, name: 'Brands' }

  given(:taxon_1) { create :taxon, name: 'C1', taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id }
  given(:taxon_2) { create :taxon, name: 'C2', taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id }
  given(:taxon_3) { create :taxon, name: 'C3', taxonomy: taxonomy_2, parent_id: taxonomy_2.root.id }
  given(:taxon_4) { create :taxon, name: 'C4', taxonomy: taxonomy_2, parent_id: taxon_1.id }

  yesterday = Time.current.ago(1.day)
  given!(:product) { create(:product, taxons: [taxon_1], available_on: yesterday) }
  given!(:product_same_taxon) { create(:product, taxons: [taxon_1], available_on: yesterday) }
  given!(:product_different_taxon) { create(:product, taxons: [taxon_2]) }
  given!(:product_other_taxonomy) { create(:product, taxons: [taxon_3]) }

  # 関連商品の取得数の上限をテストするため、1カテゴリ（子孫カテゴリを含む）に
  # 8件のproductが登録されるようにする(taxon_4はtaxon_1の子カテゴリ)
  given!(:product_child_taxon) { create(:product, taxons: [taxon_4], available_on: yesterday) }
  given!(:product_child_taxon_A) { create(:product, taxons: [taxon_4], available_on: yesterday) }
  given!(:product_child_taxon_B) { create(:product, taxons: [taxon_4], available_on: yesterday) }
  given!(:product_child_taxon_C) { create(:product, taxons: [taxon_4], available_on: yesterday) }
  given!(:product_child_taxon_D) { create(:product, taxons: [taxon_4], available_on: yesterday) }
  given!(:product_child_taxon_not_display) do
    create(:product, taxons: [taxon_4],
                     available_on: Time.current.ago(1.month))
  end

  before do
    visit potepan_product_path(id: product.id)
  end

  scenario "show related products exclude itself, other taxon/taxonomy. Max is 6 products" do
    within '.row.productsContent' do
      # 自身と同じカテゴリ、または子カテゴリにあるproductを取得する
      expect(page).to have_content(product_same_taxon.name.upcase)
      expect(page).to have_content(product_child_taxon.name.upcase)
      # 自分自身、他カテゴリ、Categories以外のtaxonomy配下、７件目以降の商品は関連商品として表示しない
      expect(page).to have_no_content(product.name.upcase)
      expect(page).to have_no_content(product_different_taxon.name.upcase)
      expect(page).to have_no_content(product_other_taxonomy.name.upcase)
      expect(page).to have_no_content(product_child_taxon_not_display.name.upcase)
    end
  end

  scenario "render product page when click on related product" do
    within '.row.productsContent' do
      click_on(product_same_taxon.name)
    end
    expect(page).to have_selector 'h2', text: product_same_taxon.name.upcase
  end

  scenario "show related products dynamically" do
    visit potepan_product_path(id: product_same_taxon.id)
    expect(find('.row.productsContent')).to have_content(product.name.upcase)
  end
end
