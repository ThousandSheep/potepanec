require 'rails_helper'
RSpec.feature "Choose_category_spec", type: :feature do
  given!(:taxonomy) { create :taxonomy, name: 'Categories' }

  # taxonomy に2つのtaxonを作成する。ぞれぞれのtaxonは違ったproductを持つ。
  given!(:taxon_1) { create :taxon, name: 'Cat1', taxonomy: taxonomy, parent_id: taxonomy.root.id }
  given!(:taxon_2) { create :taxon, name: 'Cat2', taxonomy: taxonomy, parent_id: taxonomy.root.id }

  given!(:product_1) { create(:product, taxons: [taxon_1]) }
  given!(:product_2) { create(:product, taxons: [taxon_1]) }
  given!(:product_3) { create(:product, taxons: [taxon_2]) }

  given!(:option_type) { create :option_type, presentation: "Color" }
  given!(:option_blue) do
    create :option_value, presentation: "Blue", option_type_id: option_type.id
  end
  given!(:option_gray) do
    create :option_value, presentation: "Gray", option_type_id: option_type.id
  end
  given!(:variant_blue) do
    create :variant, product_id: product_1.id, option_values: [option_blue]
  end
  given!(:variant_gray) do
    create :variant, product_id: product_1.id, option_values: [option_gray]
  end

  before do
    visit potepan_category_path(id: taxon_1.id)
  end

  scenario "check template" do
    expect(page).to have_title "#{taxon_1.name} - BIGBAG Store"
    expect(page).to have_selector 'h2', text: taxon_1.name.upcase
  end

  scenario "show category tree" do
    within '.side-nav' do
      expect(page).to have_content "Cat1"
      expect(page).to have_content "Cat2"
    end
  end

  scenario "show filters" do
    within '#filter_by_color' do
      expect(page).to have_content "Blue"
      expect(page).to have_content "Gray"
    end
  end

  scenario "show appropriate product with taxon_1" do
    within '#productsList' do
      taxon_1.products.each do |taxon_1_product|
        expect(page).to have_content(taxon_1_product.name.upcase)
      end
      taxon_2.products.each do |taxon_2_product|
        expect(page).to have_no_content(taxon_2_product.name.upcase)
      end
    end
  end

  scenario "render products page on click" do
    within '#productsList' do
      click_link(
        product_1.name, href: "/potepan/products/#{product_1.id}?variant=#{variant_blue.id}"
      )
    end
    expect(page).to have_content(product_1.name.upcase)
  end

  scenario "show appropriate product with taxon_2" do
    within '.side-nav' do
      # taxon_2の商品表示に切り替え
      click_on(taxon_2.name)
    end
    expect(page).to have_selector 'h2', text: taxon_2.name.upcase
    taxon_1.products.each do |taxon_1_product|
      expect(page).to have_no_content(taxon_1_product.name.upcase)
    end
    taxon_2.products.each do |taxon_2_product|
      expect(page).to have_content(taxon_2_product.name.upcase)
    end
  end

  scenario "filter by color" do
    within '.side-nav' do
      click_on(taxon_1.name)
    end
    within '#filter_by_color' do
      click_on(option_blue.presentation)
    end
    expect(page).to have_link(
      product_1.name, href: "/potepan/products/#{product_1.id}?variant=#{variant_blue.id}"
    )
    expect(page).to have_no_link(
      product_1.name, href: "/potepan/products/#{product_1.id}?variant=#{variant_gray.id}"
    )
  end
end
