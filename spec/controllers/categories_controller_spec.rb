require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxonomy) { create :taxonomy }
    let(:taxon) { create :taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id }
    let!(:option_type) { create :option_type, presentation: "Color" }
    let!(:option_value_blue) { create :option_value, name: "Blue" }
    let!(:option_value_gray) { create :option_value, name: "Gray" }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'response is 200 OK' do
      expect(response.status).to eq 200
    end

    it 'render_template :show' do
      expect(response).to render_template(:show)
    end
  end
end
