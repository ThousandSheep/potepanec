require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    before do
      get :index
    end

    it "response is 200 OK" do
      expect(response.status).to eq 200
    end

    it 'render_template :homdex' do
      expect(response).to render_template(:index)
    end
  end
end
