require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create :taxonomy, name: 'Categories' }
    let(:taxon) { create :taxon, name: 'C1', taxonomy: taxonomy, parent_id: taxonomy.root.id }
    let(:product) { create :product, taxons: [taxon] }

    before do
      get :show, params: { id: product.id }
    end

    it 'response is 200 OK' do
      expect(response.status).to eq 200
    end

    it 'render_template :show' do
      expect(response).to render_template(:show)
    end

    it 'show appropriate product' do
      expect(assigns[:product]).to eq product
    end

    it 'show appropriate images' do
      expect(assigns[:images]).to eq product.images
    end
  end
end
