file = "#{Rails.root}/config/constants.yml"

# 再帰的にオブジェクトを凍結する
def deep_freeze(hash)
  hash.freeze.each_value do |element|
    element.kind_of?(Hash) ? deep_freeze(element) : element.freeze
  end
end

CONFIG = deep_freeze(YAML.load_file(file)[Rails.env].deep_symbolize_keys)