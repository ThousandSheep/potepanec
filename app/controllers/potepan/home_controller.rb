class Potepan::HomeController < ApplicationController
  def index
    # available_onが1ヶ月以内の商品を新着商品とする
    new_arrival_term = Time.current.ago(1.month)
    @new_arrival_products = Spree::Product.where("available_on > ?", new_arrival_term)
  end
end
