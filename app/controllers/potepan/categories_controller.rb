class Potepan::CategoriesController < ApplicationController
  def show
    # サイドバーの表示
    @categories = Spree::Taxonomy.includes(:taxons)

    # FILTER BY COLORのため、色一覧を取得
    option_type_color = Spree::OptionType.find_by(presentation: "Color")
    @colors = option_type_color.option_values

    # 商品の表示
    @taxon = Spree::Taxon.find(params[:id])
    if params[:color].blank?
      @products = @taxon.all_variants.includes(:option_values)
    else
      @products = @taxon.all_variants.includes(:option_values).where(
        spree_option_values: { id: params[:color].to_i }
      )
    end

    respond_to do |format|
      format.html
      format.js
    end
  end
end
