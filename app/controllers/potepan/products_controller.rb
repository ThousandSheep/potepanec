class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    if params[:variant].nil? || params[:id] == params[:variant]
      @images = @product.images
      @display_price = @product.display_price
    else
      variant = Spree::Variant.find(params[:variant])
      @images = variant.images
      @display_price = variant.display_price
    end

    # 関連商品
    category_id_in_taxonomy = Spree::Taxonomy.find_by(name: "Categories").id
    taxon = @product.taxons.find_by(taxonomy_id: category_id_in_taxonomy)

    # 自分自身を除く同カテゴリ、子孫カテゴリ内の商品を取得(新着順、最大6件)
    @related_products = taxon.all_products.includes(
      master: [:default_price, :images]
    ).where.not(id: @product.id).order(available_on: "DESC").
      distinct.
      limit(CONFIG[:MAX_RELATED_PRODUCTS_GET_FROM_DB])
  end
end
