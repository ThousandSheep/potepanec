module ApplicationHelper
  # variantsのサイズを表示する
  def show_size(variant)
    # optionsには["Size: A", " Color: B"] の形でオプションが格納される
    options = variant.options_text.split(",")
    options.select { |element| element.include?("Size") }
  end
end
